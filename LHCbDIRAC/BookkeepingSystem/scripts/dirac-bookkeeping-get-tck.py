#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
########################################################################
# File :    dirac-bookkeeping-get-tck
# Author :  Zoltan Mathe
########################################################################
"""Returns list of TCKs for a run range, by default only if there is a FULL stream."""

__RCSID__ = "$Id$"

from LHCbDIRAC.DataManagementSystem.Client.DMScript import Script

if __name__ == "__main__":
  Script.registerSwitch('', 'Runs=', 'Run range or list')
  Script.registerSwitch('', 'ByRange', 'List by range rather than by item value')
  Script.registerSwitch('', 'Force', 'Include runs even if no FULL stream is present')
  Script.registerSwitch('', 'DQFlag=', 'Specify the DQ flag (default: all)')
  Script.setUsageMessage(__doc__ + '\n'.join([
      'Usage:',
      '  %s [option|cfgfile] ... ' % Script.scriptName]))

  Script.parseCommandLine(ignoreErrors=True)

  from LHCbDIRAC.BookkeepingSystem.Client.ScriptExecutors import executeRunInfo
  executeRunInfo('Tck')
