Citations
  .. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.1451768.svg
   :target: https://doi.org/10.5281/zenodo.1451768

Documentation
  .. image:: https://readthedocs.org/projects/lhcb-dirac/badge
      :target: http://lhcb-dirac.readthedocs.io/

Master
  .. image:: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/badges/master/pipeline.svg
      :target: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/commits/master
  .. image:: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/badges/master/coverage.svg
      :target: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/commits/master

Devel
  .. image:: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/badges/devel/pipeline.svg
          :target: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/commits/devel
  .. image:: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/badges/devel/coverage.svg
          :target: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/commits/devel

See the full `coverage output`_.
  .. _`coverage output`: https://lhcbdirac-cov.web.cern.ch/


LHCbDIRAC is the LHCb extension of [DIRAC](https://github.com/DIRACGrid/DIRAC).

Important links
===============

- Official source code repo: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC
- Issue tracker: https://its.cern.ch/jira/browse/LHCBDIRAC/
- Developers Mailing list: https://groups.cern.ch/group/lhcb-dirac/default.aspx
- KB articles about GitLab usage: https://cern.service-now.com/service-portal/topic.do?topic=Gitlab&s=it

Install
=======

For more detailed installation instructions, see the DIRAC documentation.
