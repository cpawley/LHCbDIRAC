====================
DBs in certification
====================

How-To in `lbDevOps doc <https://lbdevops.web.cern.ch/lbdevops/DIRACInfrastructure.html>`_.


ElasticSearch
=============

All ES data for certification is stored in **es-lhcb-dev.cern.ch** at port 9203, and visualized via kibana in `<https://es-lhcb-dev.cern.ch/kibana/app/kibana#/home?_g=()>`_

<TODO: expand on connection details>